package com.example.acratest


import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.widget.Toast
import com.github.anrwatchdog.ANRWatchDog
import org.acra.ACRA
import org.acra.BuildConfig
import org.acra.ReportField
import org.acra.annotation.AcraCore
import org.acra.annotation.AcraHttpSender
import org.acra.annotation.AcraToast
import org.acra.config.httpSender
import org.acra.config.toast
import org.acra.data.StringFormat
import org.acra.ktx.initAcra
import org.acra.sender.HttpSender
import org.acra.startup.Report


class MyApplication : Application() {

    override fun attachBaseContext(base:Context) {
        super.attachBaseContext(base)

        initAcra{
            //core configuration:
            buildConfigClass = BuildConfig::class.java
            reportFormat = StringFormat.JSON

            //[OPSIONAL]
            reportContent = arrayOf(ReportField.APP_VERSION_CODE, ReportField.INSTALLATION_ID, ReportField.APP_VERSION_NAME, ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL, ReportField.CUSTOM_DATA, ReportField.STACK_TRACE,
                ReportField.LOGCAT, ReportField.USER_IP, ReportField.BUILD_CONFIG, ReportField.BRAND, ReportField.DEVICE_ID)


            httpSender {
                //required. Https recommended
                //DEV
                uri = "https://acrarium-crashreport-dev.apps.ocpdev.dti.co.id/report"
                //optional. Enables http basic auth
                basicAuthLogin = "4el6Q6LBsva9tGNk"
                //required if above set
                basicAuthPassword = "dJWLz1VufZrcxrjf"
                // defaults to POST
                httpMethod = HttpSender.Method.POST
                certificatePath = "asset://devapps.pem"


//                //defaults to 5000ms
//                connectionTimeout = 5000
//                //defaults to 20000ms
//                socketTimeout = 20000
//                // defaults to false
//                dropReportsOnTimeout = false

//                //the following options allow you to configure a self signed certificate
//                keyStoreFactoryClass = MyKeyStoreFactory::class.java

                //certificatePath = "asset://devapps.pem"
                //resCertificate = R.raw.mycert
                //certificateType = "X.509"

//                //defaults to false. Recommended if your backend supports it
//                compress = false
//                //defaults to all
//                tlsProtocols = arrayOf(TLS.V1_3, TLS.V1_2, TLS.V1_1, TLS.V1)
            }

            toast {
                text = getString(R.string.crash_string)
                length = Toast.LENGTH_LONG
                //opening this block automatically enables the plugin.
            }
        }
    }

    override fun onCreate() {
        super.onCreate()

        //ANRWatchDog().start()
        //ANRWatchDog().setIgnoreDebugger(true).start()


        ANRWatchDog().setIgnoreDebugger(true)
            .setANRListener { error -> // Handle the error. For example, log it to HockeyApp:
                ACRA.errorReporter.handleException(error)
            }.start()

        ACRA.errorReporter.putCustomData("Key ID", "TEST this is Custom Data")
        ACRA.errorReporter.putCustomData("EXTRA_INFORMATION", "Mi A1 - 7077")

        trackBreadcrumb("MyActivity.onCreate()")
    }


    //If you want the report to show "Breadcrumbs" to indicate which events happened in time order,
    // 'just before a crash, then you need to track events using unique keys. Here's an example:
    fun trackBreadcrumb(event: String) {
        ACRA.errorReporter.putCustomData("Event at " + System.currentTimeMillis(), event)
    }

}
